var app = angular.module('Login', ['ui.router']);

app.config(function($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/login');
    $stateProvider
        .state('login', {
            url: '/login',
            templateUrl: './template/login/login.html',
            controller: 'loginController'
        })
        .state('signup', {
            url: '/signup',
            templateUrl: './template/login/signup.html',
            controller: 'loginController'
        })
        .state('user/list', {
            url: '/user/list',
            templateUrl: './template/home/userList.html',
            controller: 'userController'
        })
        .state('git/repo/list', {
            url: '/git/repo/list:Name',
            templateUrl: './template/listGitRepo.html',
            controller: 'gitRepoController',
            params: {
                Name: null
            }
        })
        .state('enterName', {
            url: '/enterName',
            templateUrl: './template/enterName.html',
            controller: 'userController'
        })
        .state('address/list', {
            url: '/address/list:userID',
            templateUrl: './template/address/addressList.html',
            controller: 'addressListController',
            params: {
                userID: null
            }
        })
        .state('add/address', {
            url: '/add/address:userID',
            templateUrl: './template/address/addAddress.html',
            controller: 'addAddressController',
            params: {
                userID: null
            }
        })
        .state('logout', {
            url: '/logout',
            templateUrl: './template/login/login.html',
            controller: 'loginController'
        })
});