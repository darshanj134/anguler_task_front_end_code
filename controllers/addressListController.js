app.controller('addressListController', function($scope, $stateParams, $http, $timeout, $state, $window) {
    $scope.formData = {};
    console.info("USER ID:" + $stateParams.userID);
    localStorage.setItem("userID", $stateParams.userID);
    $http({
            method: 'GET',
            url: serverURL.baseURL + "users/address/list?id=" + $stateParams.userID,
            headers: {
                "dsmart_authorization": "ZGV2ZWxvcGVyU21hcnQ=",
                "Content-Type": "application/json",
                "x-access-token": localStorage.getItem("x-access-token")
            },
            data: $scope.userCredentials
        })
        .then(function(response) {
            console.log(response);
            if (response.data.status == true) {
                $scope.addressDetails = response.data.data
            }
        });
})