app.controller('addAddressController', function($scope, $stateParams, $http, $timeout, $state, $window) {
    $scope.formData = {};
    console.info("USER ID:" + localStorage.getItem("userID"));

    $scope.addAddress = function() {
        $scope.addressLineOne = $scope.formData.addressLineOne;
        $scope.addressLineTwo = $scope.formData.addressLineTwo;
        $scope.buildingName = $scope.formData.buildingName;
        $scope.areaName = $scope.formData.areaName;
        $scope.city = $scope.formData.city;
        $scope.state = $scope.formData.state;
        $scope.country = $scope.formData.country;
        $scope.pincode = $scope.formData.pincode;
        $scope.addressData = {
            "object_id": localStorage.getItem("userID"),
            "address_line_1": $scope.addressLineOne,
            "address_line_2": $scope.addressLineTwo,
            "building_name": $scope.buildingName,
            "area_name": $scope.areaName,
            "country": $scope.country,
            "state": $scope.state,
            "city": $scope.city,
            "pincode": $scope.pincode,
            "created_by": localStorage.getItem("userID")
        }
        console.info($scope.addressData);
        $http({
                method: 'POST',
                url: serverURL.baseURL + "users/add/address",
                headers: {
                    "dsmart_authorization": "ZGV2ZWxvcGVyU21hcnQ=",
                    "Content-Type": "application/json",
                    "x-access-token": localStorage.getItem("x-access-token")
                },
                data: $scope.addressData
            })
            .then(function(response) {
                console.log(response);
                if (response.data.status == true) {
                    $window.alert("Address Added Successfully");
                    $state.go('address/list', { 'userID': localStorage.getItem("userID") });
                    localStorage.setItem("x-access-token", response.data.token);
                }
            });
    }
})