app.controller('loginController', function($scope, $http, $timeout, $state, $window) {
    $scope.formData = {};
    $scope.salutationSelected = "";
    $scope.genderSelected = "";
    // Desc- Initializtion

    $scope.login = function() {
        console.info("CAME");
        $scope.emailID = $scope.formData.email_id;
        $scope.password = $scope.formData.password;
        console.info("EMAIL :" + $scope.emailID);
        console.info("PASS :" + $scope.password);
        $scope.userCredentials = {
            "email_id": $scope.emailID,
            "password": $scope.password
        }
        $http({
                method: 'POST',
                url: serverURL.baseURL + "validate/login",
                headers: {
                    "dsmart_authorization": "ZGV2ZWxvcGVyU21hcnQ=",
                    "Content-Type": "application/json"
                },
                data: $scope.userCredentials
            })
            .then(function(response) {
                console.log(response);
                if (response.data.status == true) {
                    $window.alert("User logged in Successfully");
                    $state.go('user/list', { reload: true });
                    localStorage.setItem("x-access-token", response.data.token);
                }
            });
    }

    // Desc - Common Methods For SHow Errors
    function showSuccessMessage(message) {
        $scope.successMesssage = message;
        $timeout(function() {
            $scope.successMesssage = "";
        }, 5000);
    }

    function showErrorMessage(message) {
        $scope.errorMesssage = message;
        $timeout(function() {
            $scope.errorMesssage = "";
        }, 5000);
    }


    $scope.salutationChange = function() {
        $scope.salutationSelected = $scope.formData.salutation;
    }

    $scope.genderChange = function() {
        $scope.genderSelected = $scope.formData.gender;
    }


    // Desc - For Register User
    $scope.registerUser = function() {
        $scope.firstName = $scope.formData.firstName;
        $scope.lastName = $scope.formData.lastName;
        $scope.emailID = $scope.formData.emailid;
        $scope.password = $scope.formData.password;
        $scope.confirmPassowrd = $scope.formData.cpassword;
        $scope.mobileNumber = $scope.formData.mobileNumber;
        $scope.userAge = $scope.formData.age;
        if ($scope.salutationSelected == "" || $scope.salutationSelected == undefined || $scope.salutationSelected == "null") {
            showErrorMessage("Please Select Salutation");
        } else if ($scope.password != $scope.confirmPassowrd) {
            showErrorMessage("Password Doesnot match");
        } else if ($scope.genderSelected == "" || $scope.genderSelected == undefined || $scope.genderSelected == null) {
            showErrorMessage("Please Select Gender");
        } else {
            $scope.registerObject = {
                "salutation": $scope.salutationSelected,
                "first_name": $scope.firstName,
                "last_name": $scope.lastName,
                "email_id": $scope.emailID,
                "mobile_number": $scope.mobileNumber,
                "age": $scope.userAge,
                "gender": $scope.genderSelected,
                "password": $scope.password
            }
            console.info($scope.registerObject);
            $http({
                    method: 'POST',
                    url: serverURL.baseURL + "users/create",
                    headers: {
                        "dsmart_authorization": "ZGV2ZWxvcGVyU21hcnQ=",
                        "Content-Type": "application/json",
                        "x-access-token": localStorage.getItem("x-access-token")
                    },
                    data: $scope.registerObject
                })
                .then(function(response) {
                    console.log(response);
                    if (response.data.status == true) {
                        $window.alert("User Registered Successfully");
                        $state.go('login', { reload: true })
                    } else {
                        $window.alert(response.message);
                    }
                });
        }
    }

})