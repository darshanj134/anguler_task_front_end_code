app.controller('userController', function($scope, $http, $timeout, $state, $window) {
    console.info("CAME TO USER LIST");
    $scope.formData = {};
    $http({
            method: 'GET',
            url: serverURL.baseURL + "users/list?id=all",
            headers: {
                "dsmart_authorization": "ZGV2ZWxvcGVyU21hcnQ=",
                "Content-Type": "application/json",
                "x-access-token": localStorage.getItem("x-access-token")
            },
            data: $scope.userCredentials
        })
        .then(function(response) {
            console.log(response);
            if (response.data.status == true) {
                $scope.userDetails = response.data.data
            }
        });

    $scope.search = function() {
        console.info($scope.formData.userName);
        if ($scope.formData.userName == undefined || $scope.formData.userName == "") {
            $window.alert("Please Enter Username");
        } else {
            $state.go('git/repo/list', { 'Name': $scope.formData.userName })
        }
    }
})