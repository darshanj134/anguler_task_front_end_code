app.controller('gitRepoController', function($scope, $http, $stateParams, $timeout, $state, $window) {
    console.info("CAME TO GIT CONTROLLER");
    $scope.formData = {};
    $scope.isNoRecords = false;
    console.info($stateParams.Name);

    $http({
            method: 'GET',
            url: "https://api.github.com/users/" + $stateParams.Name + "/repos"
        })
        .then(function(response) {
            console.log(response);
            $scope.getRepoDetails = response.data
            if ($scope.getRepoDetails == "" || $scope.getRepoDetails == "null" || $scope.getRepoDetails == undefined || $scope.getRepoDetails == null) {
                $scope.isNoRecords == true;
            } else {
                $scope.isNoRecords == false;
            }
        });
})